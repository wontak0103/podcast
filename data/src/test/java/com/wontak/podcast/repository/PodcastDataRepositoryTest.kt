package com.wontak.podcast.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wontak.podcast.local.dao.PodcastDao
import com.wontak.podcast.remote.PodcastApiService
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class PodcastDataRepositoryTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: PodcastRepository
    private lateinit var dao: PodcastDao
    private lateinit var service: PodcastApiService

    @Before
    fun init() {
        dao = mock(PodcastDao::class.java)
        service = mock(PodcastApiService::class.java)
        repository = PodcastDataRepository(dao, service)
    }

    @Test
    fun `load podcast from network`() {

    }
}