package com.wontak.podcast.local.mapper

import com.wontak.podcast.local.model.Episode
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

class EpisodeMapperTest {

    @Test
    fun map() {
        val mapper = EpisodeMapper()
        val original = Episode(
            "http://example.com/podcasts/archive/aae20140615.m4a",
            100L,
            "Shake Shake Shake Your Spices",
            "A short primer on table spices",
            1234L,
            "http://example.com/podcasts/everything/AllAboutEverything/Episode1.jpg",
            "http://example.com/podcasts/everything/AllAboutEverythingEpisode3.m4a",
            "audio/x-m4a",
            1111L,
            true
        )
        val converted = mapper.map(original)

        Assert.assertThat(converted, notNullValue())
        Assert.assertThat(converted.guid, `is`("http://example.com/podcasts/archive/aae20140615.m4a"))
        Assert.assertThat(converted.podcastId, `is`(100L))
        Assert.assertThat(converted.title, `is`("Shake Shake Shake Your Spices"))
        Assert.assertThat(converted.description, `is`("A short primer on table spices"))
        Assert.assertThat(converted.duration, `is`(1111L))
        Assert.assertThat(converted.updatedAt, `is`(1234L))
        Assert.assertThat(converted.explicit, `is`(true))
        Assert.assertThat(converted.fileUrl, `is`("http://example.com/podcasts/everything/AllAboutEverythingEpisode3.m4a"))
        Assert.assertThat(converted.fileType, `is`("audio/x-m4a"))
    }
}