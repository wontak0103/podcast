package com.wontak.podcast.local

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Assert
import org.junit.Test

class ObjectTypeConvertersTest {

    private val texts = listOf(
        "1,2,3,4,5",
        "5,4,3,2,1",
        "3,2,1,4,5"
    )
    private val lists = listOf(
        listOf(1, 2, 3, 4, 5),
        listOf(5, 4, 3, 2, 1),
        listOf(3, 2, 1, 4, 5)
    )

    @Test
    fun `convert string to int list`() {
        for ((index, text) in texts.withIndex()) {
            val result = ObjectTypeConverters.stringToIntList(text)
            Assert.assertThat(result, notNullValue())
            Assert.assertThat(result?.equals(lists[index]), `is`(true))
        }
    }

    @Test
    fun `convert wrong formatted string to int list`() {
        val result = ObjectTypeConverters.stringToIntList("[1, 2, 3, 4, 5]")
        Assert.assertThat(result, notNullValue())
        Assert.assertThat(result?.size, `is`(0))
    }

    @Test
    fun `convert int list to string`() {
        for ((index, list) in lists.withIndex()) {
            val result = ObjectTypeConverters.intListToString(list)
            Assert.assertThat(result, notNullValue())
            Assert.assertThat(result?.equals(texts[index]), `is`(true))
        }
    }
}