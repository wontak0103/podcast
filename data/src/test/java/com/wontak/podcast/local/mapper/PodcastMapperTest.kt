package com.wontak.podcast.local.mapper

import com.wontak.podcast.local.model.Podcast
import com.wontak.podcast.model.Category
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.junit.Assert
import org.junit.Test

class PodcastMapperTest {

    @Test
    fun map() {
        val mapper = PodcastMapper()
        val original = Podcast(
            100L,
            "All About Everything",
            "John Doe",
            "A show about everything",
            "en-us",
            "http://www.example.com/podcasts/everything/index.html",
            "http://example.com/podcasts/everything/AllAboutEverything.jpg",
            listOf(1309, 1301, 1306),
            false
        )
        val converted = mapper.map(original)

        Assert.assertThat(converted, notNullValue())
        Assert.assertThat(converted.id, `is`(100L))
        Assert.assertThat(converted.title, `is`("All About Everything"))
        Assert.assertThat(converted.author, `is`("John Doe"))
        Assert.assertThat(converted.description, `is`("A show about everything"))
        Assert.assertThat(converted.language, `is`("en-us"))
        Assert.assertThat(converted.link, `is`("http://www.example.com/podcasts/everything/index.html"))
        Assert.assertThat(converted.imageUrl, `is`("http://example.com/podcasts/everything/AllAboutEverything.jpg"))
        Assert.assertThat(converted.explicit, `is`(false))

        val categories = converted.categories
        Assert.assertThat(categories, notNullValue())
        Assert.assertThat(categories.size, `is`(3))
        Assert.assertThat(categories[0], `is`(Category.TV_AND_FILM))
        Assert.assertThat(categories[1], `is`(Category.ARTS))
        Assert.assertThat(categories[2], `is`(Category.FOOD))
    }
}