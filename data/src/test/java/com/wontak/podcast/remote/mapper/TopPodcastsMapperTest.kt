package com.wontak.podcast.remote.mapper

import com.wontak.podcast.remote.model.TopPodcasts
import com.wontak.podcast.remote.model.atom.iTunesAttributes
import com.wontak.podcast.remote.model.atom.iTunesEntry
import com.wontak.podcast.remote.model.atom.iTunesFeed
import com.wontak.podcast.remote.model.atom.iTunesLabel
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert
import org.junit.Test

class TopPodcastsMapperTest {

    @Test
    fun map() {
        val mapper = TopPodcastsMapper()
        val original = TopPodcasts(
            iTunesFeed(
                listOf(
                    iTunesEntry(
                        iTunesLabel("https://itunes.apple.com/us/podcast/post-reports/id1444873564?mt=2&uo=2", iTunesAttributes("1444873564")),
                        iTunesLabel("Post Reports", iTunesAttributes("")),
                        iTunesLabel("The Washington Post", iTunesAttributes("")),
                        iTunesLabel(
                            "Post Reports is the daily podcast from The Washington Post. Unparalleled reporting. " +
                            "Expert insight. Clear analysis. Everything you’ve come to expect from the newsroom of The Post. " +
                            "For your ears. Martine Powers is your host, asking the questions you didn’t know you wanted answered.",
                            iTunesAttributes("")
                        ),
                        listOf(
                            iTunesLabel(
                                "https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/55x55bb-85.png",
                                iTunesAttributes("")
                            ),
                            iTunesLabel(
                                "https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/60x60bb-85.png",
                                iTunesAttributes("")
                            ),
                            iTunesLabel(
                                "https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/170x170bb-85.png",
                                iTunesAttributes("")
                            )
                        ),
                        iTunesLabel("", iTunesAttributes("1311"))
                    )
                )
            )
        )
        val converted = mapper.map(original)

        Assert.assertThat(converted, notNullValue())
        Assert.assertThat(converted.size, `is`(1))

        val chart = converted[0]
        Assert.assertThat(chart.id, `is`(1444873564L))
        Assert.assertThat(chart.title, `is`("Post Reports"))
        Assert.assertThat(chart.auther, `is`("The Washington Post"))
        Assert.assertThat(chart.summary, `is`("Post Reports is the daily podcast from The Washington Post. Unparalleled reporting. " +
                "Expert insight. Clear analysis. Everything you’ve come to expect from the newsroom of The Post. " +
                "For your ears. Martine Powers is your host, asking the questions you didn’t know you wanted answered."))
        Assert.assertThat(chart.imageBaseUrl, `is`("https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png"))
    }
}