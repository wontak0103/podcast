package com.wontak.podcast.remote

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wontak.podcast.model.Category
import com.wontak.podcast.util.TestUtils
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.core.IsNull.notNullValue
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class iTunesSearchApiServiceTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: iTunesSearchApiService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(iTunesSearchApiService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun getTopAudioPodcasts() {
        TestUtils.enqueueMockResponse(mockWebServer, "topaudiopodcasts.json")
        val testObserver = service.getTopAudioPodcasts("us", Category.ALL.id, 10).test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val request = mockWebServer.takeRequest()
        val result = testObserver.values()[0]

        Assert.assertThat(request.path, `is`("/us/rss/topaudiopodcasts/limit=10/genre=26/json"))
        Assert.assertThat(result, notNullValue())

        val feed = result.feed
        Assert.assertThat(feed, notNullValue())
        Assert.assertThat(feed.entries.size, `is`(10))

        val entry = feed.entries[0]
        Assert.assertThat(entry, notNullValue())
        Assert.assertThat(entry.id.attributes.id, `is`("1444873564"))
        Assert.assertThat(entry.name.label, `is`("Post Reports"))
        Assert.assertThat(entry.artist.label, `is`("The Washington Post"))
        Assert.assertThat(entry.summary.label, `is`("Post Reports is the daily podcast from The Washington Post. Unparalleled reporting. Expert insight. Clear analysis. Everything you’ve come to expect from the newsroom of The Post. For your ears. Martine Powers is your host, asking the questions you didn’t know you wanted answered."))
        Assert.assertThat(entry.images.size, `is`(3))
        Assert.assertThat(entry.images[0].label, `is`("https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/55x55bb-85.png"))
        Assert.assertThat(entry.images[1].label, `is`("https://is5-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/60x60bb-85.png"))
        Assert.assertThat(entry.images[2].label, `is`("https://is4-ssl.mzstatic.com/image/thumb/Podcasts118/v4/2d/86/09/2d860904-6d10-8c16-6421-873b11be37bc/mza_2979978341436700282.png/170x170bb-85.png"))
        Assert.assertThat(entry.category.attributes.id, `is`("1311"))
    }

    @Test
    fun search() {
        TestUtils.enqueueMockResponse(mockWebServer, "searchresults.json")
        val testObserver = service.search("cnn").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val request = mockWebServer.takeRequest()
        val result = testObserver.values()[0]

        Assert.assertThat(request.path, `is`("/search?term=cnn&limit=50&entity=podcast"))
        Assert.assertThat(result, notNullValue())
        Assert.assertThat(result.resultCount, `is`(50))
        Assert.assertThat(result.results.size, `is`(50))

        val data = result.results[0]

        Assert.assertThat(data.wrapperType, `is`("track"))
        Assert.assertThat(data.kind, `is`("podcast"))
        Assert.assertThat(data.artistId, `is`(148813963L))
        Assert.assertThat(data.collectionId, `is`(1043593599L))
        Assert.assertThat(data.trackId, `is`(1043593599L))
        Assert.assertThat(data.artistName, `is`("CNN"))
        Assert.assertThat(data.collectionName, `is`("The Axe Files with David Axelrod"))
        Assert.assertThat(data.trackName, `is`("The Axe Files with David Axelrod"))
        Assert.assertThat(data.collectionCensoredName, `is`("The Axe Files with David Axelrod"))
        Assert.assertThat(data.trackCensoredName, `is`("The Axe Files with David Axelrod"))
        Assert.assertThat(data.artistViewUrl, `is`("https://itunes.apple.com/us/artist/cnn/148813963?mt=2&uo=4"))
        Assert.assertThat(data.collectionViewUrl, `is`("https://itunes.apple.com/us/podcast/the-axe-files-with-david-axelrod/id1043593599?mt=2&uo=4"))
        Assert.assertThat(data.feedUrl, `is`("https://rss.art19.com/axe-files"))
        Assert.assertThat(data.trackViewUrl, `is`("https://itunes.apple.com/us/podcast/the-axe-files-with-david-axelrod/id1043593599?mt=2&uo=4"))
        Assert.assertThat(data.artworkUrl30, `is`("https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/bc/2f/ca/bc2fca37-8ed8-12fb-c584-d8fa4b8448fa/source/30x30bb.jpg"))
        Assert.assertThat(data.artworkUrl60, `is`("https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/bc/2f/ca/bc2fca37-8ed8-12fb-c584-d8fa4b8448fa/source/60x60bb.jpg"))
        Assert.assertThat(data.artworkUrl100, `is`("https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/bc/2f/ca/bc2fca37-8ed8-12fb-c584-d8fa4b8448fa/source/100x100bb.jpg"))
        Assert.assertThat(data.artworkUrl600, `is`("https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/bc/2f/ca/bc2fca37-8ed8-12fb-c584-d8fa4b8448fa/source/600x600bb.jpg"))
        Assert.assertThat(data.collectionPrice, `is`(0.00f))
        Assert.assertThat(data.trackPrice, `is`(0.00f))
        Assert.assertThat(data.trackRentalPrice, `is`(0f))
        Assert.assertThat(data.collectionHdPrice, `is`(0f))
        Assert.assertThat(data.trackHdPrice, `is`(0f))
        Assert.assertThat(data.trackHdRentalPrice, `is`(0f))
        Assert.assertThat(data.releaseDate, `is`("2018-12-20T09:00:00Z"))
        Assert.assertThat(data.collectionExplicitness, `is`("notExplicit"))
        Assert.assertThat(data.trackExplicitness, `is`("notExplicit"))
        Assert.assertThat(data.trackCount, `is`(298))
        Assert.assertThat(data.country, `is`("USA"))
        Assert.assertThat(data.currency, `is`("USD"))
        Assert.assertThat(data.primaryGenreName, `is`("News & Politics"))
        Assert.assertThat(data.genreIds, notNullValue())
        Assert.assertThat(data.genreIds[0], `is`("1311"))
        Assert.assertThat(data.genreIds[1], `is`("26"))
        Assert.assertThat(data.genres, notNullValue())
        Assert.assertThat(data.genres[0], `is`("News & Politics"))
        Assert.assertThat(data.genres[1], `is`("Podcasts"))
    }

    @Test
    fun lookup() {
        TestUtils.enqueueMockResponse(mockWebServer, "lookupresults.json")
        val testObserver = service.lookup(430333725L).test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val request = mockWebServer.takeRequest()
        val result = testObserver.values()[0]

        Assert.assertThat(request.path, `is`("/lookup?id=430333725"))
        Assert.assertThat(result, notNullValue())
        Assert.assertThat(result.resultCount, `is`(1))
        Assert.assertThat(result.results.size, `is`(1))

        val data = result.results[0]

        Assert.assertThat(data.wrapperType, `is`("track"))
        Assert.assertThat(data.kind, `is`("podcast"))
        Assert.assertThat(data.artistId, `is`(364380278L))
        Assert.assertThat(data.collectionId, `is`(917918570L))
        Assert.assertThat(data.trackId, `is`(917918570L))
        Assert.assertThat(data.artistName, `is`("This American Life"))
        Assert.assertThat(data.collectionName, `is`("Serial"))
        Assert.assertThat(data.trackName, `is`("Serial"))
        Assert.assertThat(data.collectionCensoredName, `is`("Serial"))
        Assert.assertThat(data.trackCensoredName, `is`("Serial"))
        Assert.assertThat(data.artistViewUrl, `is`("https://itunes.apple.com/us/artist/wbez/364380278?uo=4"))
        Assert.assertThat(data.collectionViewUrl, `is`("https://itunes.apple.com/us/podcast/serial/id917918570?mt=2&uo=4"))
        Assert.assertThat(data.feedUrl, `is`("http://feeds.serialpodcast.org/serialpodcast"))
        Assert.assertThat(data.trackViewUrl, `is`("https://itunes.apple.com/us/podcast/serial/id917918570?mt=2&uo=4"))
        Assert.assertThat(data.artworkUrl30, `is`("https://is4-ssl.mzstatic.com/image/thumb/Music71/v4/61/59/94/615994ff-21b5-9817-3e89-09b7e012336d/source/30x30bb.jpg"))
        Assert.assertThat(data.artworkUrl60, `is`("https://is4-ssl.mzstatic.com/image/thumb/Music71/v4/61/59/94/615994ff-21b5-9817-3e89-09b7e012336d/source/60x60bb.jpg"))
        Assert.assertThat(data.artworkUrl100, `is`("https://is4-ssl.mzstatic.com/image/thumb/Music71/v4/61/59/94/615994ff-21b5-9817-3e89-09b7e012336d/source/100x100bb.jpg"))
        Assert.assertThat(data.artworkUrl600, `is`("https://is4-ssl.mzstatic.com/image/thumb/Music71/v4/61/59/94/615994ff-21b5-9817-3e89-09b7e012336d/source/600x600bb.jpg"))
        Assert.assertThat(data.collectionPrice, `is`(0.00f))
        Assert.assertThat(data.trackPrice, `is`(0.00f))
        Assert.assertThat(data.trackRentalPrice, `is`(0f))
        Assert.assertThat(data.collectionHdPrice, `is`(0f))
        Assert.assertThat(data.trackHdPrice, `is`(0f))
        Assert.assertThat(data.trackHdRentalPrice, `is`(0f))
        Assert.assertThat(data.releaseDate, `is`("2018-11-15T10:30:00Z"))
        Assert.assertThat(data.collectionExplicitness, `is`("notExplicit"))
        Assert.assertThat(data.trackExplicitness, `is`("notExplicit"))
        Assert.assertThat(data.trackCount, `is`(36))
        Assert.assertThat(data.country, `is`("USA"))
        Assert.assertThat(data.currency, `is`("USD"))
        Assert.assertThat(data.primaryGenreName, `is`("News & Politics"))
        Assert.assertThat(data.genreIds, notNullValue())
        Assert.assertThat(data.genreIds[0], `is`("1311"))
        Assert.assertThat(data.genreIds[1], `is`("26"))
        Assert.assertThat(data.genres, notNullValue())
        Assert.assertThat(data.genres[0], `is`("News & Politics"))
        Assert.assertThat(data.genres[1], `is`("Podcasts"))
    }
}