package com.wontak.podcast.remote

import com.wontak.podcast.remote.model.rss.iTunesRssConverterFactory
import com.wontak.podcast.util.TestUtils
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.*
import org.hamcrest.core.IsNull.notNullValue
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

@RunWith(JUnit4::class)
class PodcastApiServiceTest {

    private lateinit var service: PodcastApiService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()

        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(iTunesRssConverterFactory.create())
            .build()
            .create(PodcastApiService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `parse basic podcast rss`() {
        TestUtils.enqueueMockResponse(mockWebServer, "podcast-rss-basic.xml")
        val testObserver = service.get("/").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val result = testObserver.values()[0]
        Assert.assertThat(result, notNullValue())

        val channel = result.channel
        Assert.assertThat(channel, notNullValue())
        Assert.assertThat(channel?.title, `is`("All About Everything"))
        Assert.assertThat(channel?.link, `is`("http://www.example.com/podcasts/everything/index.html"))
        Assert.assertThat(channel?.language, `is`("en-us"))
        Assert.assertThat(channel?.copyright, `is`("℗ & © 2014 John Doe & Family"))
        Assert.assertThat(channel?.subtitle, `is`("A show about everything"))
        Assert.assertThat(channel?.author, `is`("John Doe"))
        Assert.assertThat(channel?.summary, `is`("All About Everything is a show about everything. Each week we dive into any subject known to man and talk about it as much as we can. Look for our podcast in the Podcasts app or in the iTunes Store"))
        Assert.assertThat(channel?.description, `is`("All About Everything is a show about everything. Each week we dive into any subject known to man and talk about it as much as we can. Look for our podcast in the Podcasts app or in the iTunes Store"))
        Assert.assertThat(channel?.image, `is`("http://example.com/podcasts/everything/AllAboutEverything.jpg"))
        Assert.assertThat(channel?.explicit, `is`(false))

        val owner = channel?.owner
        Assert.assertThat(owner?.name, `is`("John Doe"))
        Assert.assertThat(owner?.email, `is`("john.doe@example.com"))

        val categories = channel?.categories
        Assert.assertThat(categories, notNullValue())
        Assert.assertThat(categories?.size, `is`(5))
        Assert.assertThat(categories?.get(0), `is`("Technology"))
        Assert.assertThat(categories?.get(1), `is`("Gadgets"))
        Assert.assertThat(categories?.get(2), `is`("TV & Film"))
        Assert.assertThat(categories?.get(3), `is`("Arts"))
        Assert.assertThat(categories?.get(4), `is`("Food"))

        val items = channel?.items
        Assert.assertThat(items, notNullValue())
        Assert.assertThat(items?.size, `is`(4))
        Assert.assertThat(items?.get(0)?.title, `is`("Shake Shake Shake Your Spices"))
        Assert.assertThat(items?.get(0)?.author, `is`("John Doe"))
        Assert.assertThat(items?.get(0)?.description, nullValue())
        Assert.assertThat(items?.get(0)?.subtitle, `is`("A short primer on table spices"))
        Assert.assertThat(items?.get(0)?.summary, `is`("This week we talk about\n                <a href=\"https://itunes/apple.com/us/book/antique-trader-salt-pepper/id429691295?mt=11\">salt and pepper shakers</a>\n                , comparing and contrasting pour rates, construction materials, and overall aesthetics. Come and join the party!"))
        Assert.assertThat(items?.get(0)?.image, `is`("http://example.com/podcasts/everything/AllAboutEverything/Episode1.jpg"))
        Assert.assertThat(items?.get(0)?.enclosure?.url, `is`("http://example.com/podcasts/everything/AllAboutEverythingEpisode3.m4a"))
        Assert.assertThat(items?.get(0)?.enclosure?.mimeType, `is`("audio/x-m4a"))
        Assert.assertThat(items?.get(0)?.enclosure?.length, `is`(8727310L))
        Assert.assertThat(items?.get(0)?.guid, `is`("http://example.com/podcasts/archive/aae20140615.m4a"))
        Assert.assertThat(items?.get(0)?.pubDate, `is`("Tue, 08 Mar 2016 12:00:00 GMT"))
        Assert.assertThat(items?.get(0)?.duration, `is`("07:04"))
        Assert.assertThat(items?.get(0)?.explicit, `is`(false))
    }

    @Test
    fun `parse feedburner podcast rss`() {
        TestUtils.enqueueMockResponse(mockWebServer, "podcast-rss-feedburner.xml")
        val testObserver = service.get("/").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val result = testObserver.values()[0]
        Assert.assertThat(result, notNullValue())

        val channel = result.channel
        Assert.assertThat(channel, notNullValue())
        Assert.assertThat(channel?.title, `is`("Post Reports"))
        Assert.assertThat(channel?.link, `is`("https://www.washingtonpost.com/podcasts/post-reports"))
        Assert.assertThat(channel?.language, `is`("en-us"))
        Assert.assertThat(channel?.copyright, `is`("© The Washington Post"))
        Assert.assertThat(channel?.subtitle, `is`("Post Reports"))
        Assert.assertThat(channel?.author, `is`("The Washington Post"))
        Assert.assertThat(channel?.summary, `is`("Post Reports is the daily podcast from The Washington Post. Unparalleled reporting. Expert insight. Clear analysis. Everything youâ€™ve come to expect from the newsroom of The Post. For your ears. Martine Powers is your host, asking the questions you didnâ€™t know you wanted answered."))
        Assert.assertThat(channel?.description, `is`("Post Reports is the daily podcast from The Washington Post. Unparalleled reporting. Expert insight. Clear analysis. Everything youâ€™ve come to expect from the newsroom of The Post. For your ears. Martine Powers is your host, asking the questions you didnâ€™t know you wanted answered."))
        Assert.assertThat(channel?.image, `is`("https://podcast.posttv.com/series/20181119/t_1542663140747_name_PR_Series_Cover_Image.png"))
        Assert.assertThat(channel?.explicit, `is`(false))

        val owner = channel?.owner
        Assert.assertThat(owner?.name, `is`("The Washington Post"))
        Assert.assertThat(owner?.email, `is`("jessica.stahl@washpost.com"))

        val categories = channel?.categories
        Assert.assertThat(categories, notNullValue())
        Assert.assertThat(categories?.size, `is`(1))
        Assert.assertThat(categories?.get(0), `is`("News & Politics"))

        val items = channel?.items
        Assert.assertThat(items, notNullValue())
        Assert.assertThat(items?.size, `is`(4))
        Assert.assertThat(items?.get(0)?.title, `is`("The midterm election that's still not over"))
        Assert.assertThat(items?.get(0)?.author, `is`("The Washington Post"))
        Assert.assertThat(items?.get(0)?.description, `is`("An investigation into possible election fraud in North Carolina, the dismantling of the Consumer Financial Protection Bureau, and one former president says goodbye to another â€” his dad."))
        Assert.assertThat(items?.get(0)?.subtitle, `is`("An investigation into possible election fraud in North Carolina, the dismantling of the Consumer Financial Protection Bureau, and one former president says goodbye to another â€” his dad."))
        Assert.assertThat(items?.get(0)?.summary, `is`("An investigation into possible election fraud in North Carolina, the dismantling of the Consumer Financial Protection Bureau, and one former president says goodbye to another â€” his dad."))
        Assert.assertThat(items?.get(0)?.image, `is`("https://podcast.posttv.com/podcast/20181205/t_1544045456565_name_PR_Series_Cover_Image.png"))
        Assert.assertThat(items?.get(0)?.enclosure?.url, `is`("https://www.podtrac.com/pts/redirect.mp3/podcast.posttv.com/washpost-production/5bda0d64e4b02752889d6aaf/20181205/5c084390e4b06ac85d0eaaf0/5c084434e4b078f0fafe3747_1351620000001-300040_t_1544045622739_44100_128_2.mp3"))
        Assert.assertThat(items?.get(0)?.enclosure?.mimeType, `is`("audio/mpeg"))
        Assert.assertThat(items?.get(0)?.enclosure?.length, `is`(23228153L))
        Assert.assertThat(items?.get(0)?.guid, `is`("5c084390e4b06ac85d0eaaf0"))
        Assert.assertThat(items?.get(0)?.pubDate, `is`("Wed, 05 Dec 2018 21:30:56 +0000"))
        Assert.assertThat(items?.get(0)?.duration, `is`("1452"))
        Assert.assertThat(items?.get(0)?.explicit, `is`(false))
    }

    @Test
    fun `parse cnn podcast rss via url`() {
        val testObserver = service.get("https://rss.art19.com/axe-files").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val result = testObserver.values()[0]
        Assert.assertThat(result, notNullValue())

        val channel = result.channel
        Assert.assertThat(channel, notNullValue())
        Assert.assertThat(channel?.author, `is`("CNN"))

        val categories = channel?.categories
        Assert.assertThat(categories, notNullValue())
        Assert.assertThat(categories?.size, `is`(1))
        Assert.assertThat(categories?.get(0), `is`("News & Politics"))

        val items = channel?.items
        Assert.assertThat(items, notNullValue())
        Assert.assertThat(items?.size, not(0))
    }
}