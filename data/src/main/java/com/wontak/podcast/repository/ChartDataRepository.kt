package com.wontak.podcast.repository

import com.wontak.podcast.model.Category
import com.wontak.podcast.model.Chart
import com.wontak.podcast.remote.iTunesSearchApiService
import com.wontak.podcast.remote.mapper.TopPodcastsMapper
import io.reactivex.Single
import javax.inject.Inject

class ChartDataRepository @Inject constructor(
    val iTunesSearchApiService: iTunesSearchApiService,
    val topPodcastsMapper: TopPodcastsMapper = TopPodcastsMapper()
) : ChartRepository {

    override fun getTopAudioPodcasts(contryCode: String, category: Category, limit: Int): Single<List<Chart>> {
        return iTunesSearchApiService.getTopAudioPodcasts(contryCode, category.id, limit)
            .map { topPodcastsMapper.map(it) }
    }
}