package com.wontak.podcast.repository

import com.wontak.podcast.local.dao.PodcastDao
import com.wontak.podcast.model.Chart
import com.wontak.podcast.model.Podcast
import com.wontak.podcast.remote.PodcastApiService
import com.wontak.podcast.remote.iTunesSearchApiService
import io.reactivex.Single
import javax.inject.Inject

class PodcastDataRepository @Inject constructor(
    val podcastDao: PodcastDao,
    val podcastApiService: PodcastApiService
) : PodcastRepository {

    override fun getPodcast(id: Long): Single<Podcast> {
        return Single.error(Exception())
    }
}