package com.wontak.podcast.local

import androidx.room.TypeConverter
import timber.log.Timber

object ObjectTypeConverters {

    @TypeConverter
    @JvmStatic
    fun stringToIntList(value: String?): List<Int>? {
        return value?.let {
            it.split(",").map {
                try {
                    it.toInt()
                } catch (ex: NumberFormatException) {
                    Timber.e(ex, "Cannot convert $it to number")
                    null
                }
            }
        }?.filterNotNull()
    }

    @TypeConverter
    @JvmStatic
    fun intListToString(value: List<Int>?): String? {
        return value?.joinToString(",")
    }
}
