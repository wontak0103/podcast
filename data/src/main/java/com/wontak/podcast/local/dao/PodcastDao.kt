package com.wontak.podcast.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.wontak.podcast.local.model.Podcast
import io.reactivex.Single

@Dao
interface PodcastDao {

    @Insert
    fun insert(podcast: Podcast)

    @Query("SELECT * FROM Podcast WHERE id = :id")
    fun load(id: Long): Single<Podcast>
}
