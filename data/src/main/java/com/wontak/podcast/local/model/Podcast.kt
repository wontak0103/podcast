package com.wontak.podcast.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.wontak.podcast.local.ObjectTypeConverters

@Entity(tableName = "Podcast")
@TypeConverters(ObjectTypeConverters::class)
data class Podcast(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Long,
    @ColumnInfo(name = "title")
    var title: String,
    @ColumnInfo(name = "author")
    var author: String,
    @ColumnInfo(name = "description")
    var description: String,
    @ColumnInfo(name = "language")
    var language: String,
    @ColumnInfo(name = "link")
    var link: String,
    @ColumnInfo(name = "imageurl")
    var imageUrl: String,
    @ColumnInfo(name = "categories")
    var categories: List<Int>,
    @ColumnInfo(name = "explicit")
    var explicit: Boolean
)