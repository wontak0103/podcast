package com.wontak.podcast.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wontak.podcast.local.dao.EpisodeDao
import com.wontak.podcast.local.dao.PodcastDao
import com.wontak.podcast.local.model.Episode
import com.wontak.podcast.local.model.Podcast

@Database(entities = [Podcast::class, Episode::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun podcastDao(): PodcastDao
    abstract fun episodeDao(): EpisodeDao
}