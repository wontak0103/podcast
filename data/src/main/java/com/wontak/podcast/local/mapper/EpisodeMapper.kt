package com.wontak.podcast.local.mapper

import com.wontak.podcast.local.model.Episode
import com.wontak.podcast.mapper.base.Mapper

class EpisodeMapper : Mapper<Episode, com.wontak.podcast.model.Episode> {

    override fun map(from: Episode): com.wontak.podcast.model.Episode = com.wontak.podcast.model.Episode(
        from.guid,
        from.podcastId,
        from.title,
        from.description,
        from.duration,
        from.pubDate,
        from.explicit,
        from.fileUrl,
        from.fileType
    )
}
