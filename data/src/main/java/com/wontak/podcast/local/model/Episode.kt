package com.wontak.podcast.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Episode")
data class Episode(
    @PrimaryKey
    @ColumnInfo(name = "guid")
    var guid: String,
    @ColumnInfo(name = "podcastid")
    var podcastId: Long,
    @ColumnInfo(name = "title")
    var title: String,
    @ColumnInfo(name = "description")
    var description: String,
    @ColumnInfo(name = "pubdate")
    var pubDate: Long,
    @ColumnInfo(name = "imageurl")
    var imageUrl: String,
    @ColumnInfo(name = "fileurl")
    var fileUrl: String,
    @ColumnInfo(name = "filetype")
    var fileType: String,
    @ColumnInfo(name = "duration")
    var duration: Long,
    @ColumnInfo(name = "explicit")
    var explicit: Boolean
)