package com.wontak.podcast.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.wontak.podcast.local.model.Episode
import io.reactivex.Single

@Dao
interface EpisodeDao {

    @Insert
    fun insert(episode: Episode)

    @Query("SELECT * FROM Episode WHERE podcastid = :podcastid")
    fun load(podcastid: Long): Single<List<Episode>>
}
