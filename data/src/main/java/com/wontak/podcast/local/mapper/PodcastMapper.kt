package com.wontak.podcast.local.mapper

import com.wontak.podcast.local.model.Podcast
import com.wontak.podcast.mapper.base.Mapper
import com.wontak.podcast.model.Category

class PodcastMapper : Mapper<Podcast, com.wontak.podcast.model.Podcast> {

    override fun map(from: Podcast): com.wontak.podcast.model.Podcast = com.wontak.podcast.model.Podcast(
        from.id,
        from.title,
        from.author,
        from.description,
        from.language,
        from.link,
        from.imageUrl,
        Category.valueOf(from.categories),
        from.explicit
    )
}