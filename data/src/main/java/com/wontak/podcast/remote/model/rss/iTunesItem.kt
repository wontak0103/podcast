package com.wontak.podcast.remote.model.rss

data class iTunesItem(
    var title: String? = null,
    var author: String? = null,
    var description: String? = null,
    var subtitle: String? = null,
    var summary: String? = null,
    var image: String? = null,
    var enclosure: iTunesEnclosure? = null,
    var guid: String? = null,
    var pubDate: String? = null,
    var duration: String? = null,
    var explicit: Boolean = false,
    var isClosedCaptioned: Boolean = false
)