package com.wontak.podcast.remote.model.rss

data class iTunesEnclosure(
    var url: String? = null,
    var mimeType: String? = null,
    var length: Long = 0
)