package com.wontak.podcast.remote.model.rss

import okhttp3.ResponseBody
import org.xml.sax.InputSource
import retrofit2.Converter
import javax.xml.parsers.SAXParserFactory

internal class iTunesRssResponseBodyConverter : Converter<ResponseBody, iTunesFeed> {

    override fun convert(value: ResponseBody): iTunesFeed? {
        val feed: iTunesFeed?
        try {
            val parser = iTunesRssParser()
            val parserFactory = SAXParserFactory.newInstance()
            parserFactory.isNamespaceAware = true
            val saxParser = parserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            xmlReader.contentHandler = parser
            val inputSource = InputSource(value.charStream())
            xmlReader.parse(inputSource)
            feed = parser.completed
        } catch (e: Exception) {
            throw e
        } finally {
            value.close()
        }

        return feed
    }

}