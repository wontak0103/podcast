package com.wontak.podcast.remote.model

import com.google.gson.annotations.SerializedName

data class SearchResult(
    @SerializedName("wrapperType")
    var wrapperType: String,
    @SerializedName("kind")
    var kind: String,
    @SerializedName("artistId")
    var artistId: Long,
    @SerializedName("collectionId")
    var collectionId: Long,
    @SerializedName("trackId")
    var trackId: Long,
    @SerializedName("artistName")
    var artistName: String,
    @SerializedName("collectionName")
    var collectionName: String,
    @SerializedName("trackName")
    var trackName: String,
    @SerializedName("collectionCensoredName")
    var collectionCensoredName: String,
    @SerializedName("trackCensoredName")
    var trackCensoredName: String,
    @SerializedName("artistViewUrl")
    var artistViewUrl: String,
    @SerializedName("collectionViewUrl")
    var collectionViewUrl: String,
    @SerializedName("feedUrl")
    var feedUrl: String,
    @SerializedName("trackViewUrl")
    var trackViewUrl: String,
    @SerializedName("artworkUrl30")
    var artworkUrl30: String,
    @SerializedName("artworkUrl60")
    var artworkUrl60: String,
    @SerializedName("artworkUrl100")
    var artworkUrl100: String,
    @SerializedName("artworkUrl600")
    var artworkUrl600: String,
    @SerializedName("collectionPrice")
    var collectionPrice: Float,
    @SerializedName("trackPrice")
    var trackPrice: Float,
    @SerializedName("trackRentalPrice")
    var trackRentalPrice: Float,
    @SerializedName("collectionHdPrice")
    var collectionHdPrice: Float,
    @SerializedName("trackHdPrice")
    var trackHdPrice: Float,
    @SerializedName("trackHdRentalPrice")
    var trackHdRentalPrice: Float,
    @SerializedName("releaseDate")
    var releaseDate: String,
    @SerializedName("collectionExplicitness")
    var collectionExplicitness: String,
    @SerializedName("trackExplicitness")
    var trackExplicitness: String,
    @SerializedName("trackCount")
    var trackCount: Int,
    @SerializedName("country")
    var country: String,
    @SerializedName("currency")
    var currency: String,
    @SerializedName("primaryGenreName")
    var primaryGenreName: String,
    @SerializedName("genreIds")
    var genreIds: List<String>,
    @SerializedName("genres")
    var genres: List<String>
)