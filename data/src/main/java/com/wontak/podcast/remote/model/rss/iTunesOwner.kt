package com.wontak.podcast.remote.model.rss

data class iTunesOwner(
    var name: String? = null,
    var email: String? = null
)
