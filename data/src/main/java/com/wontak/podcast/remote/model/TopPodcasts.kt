package com.wontak.podcast.remote.model

import com.google.gson.annotations.SerializedName
import com.wontak.podcast.remote.model.atom.iTunesFeed

data class TopPodcasts(
    @SerializedName("feed")
    var feed: iTunesFeed
)