package com.wontak.podcast.remote.model.rss

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler

internal class iTunesRssParser : DefaultHandler() {

    private var owner: iTunesOwner? = null
    private var image: String? = null
    private var enclosure: iTunesEnclosure? = null

    private var currentChannel: iTunesChannel? = null
    private var currentItem: iTunesItem? = null
    private var builder: StringBuilder = StringBuilder()

    var completed: iTunesFeed? = null

    @Throws(SAXException::class)
    override fun startDocument() {
        super.startDocument()
    }

    @Throws(SAXException::class)
    override fun endDocument() {
        super.endDocument()
        completed = iTunesFeed(currentChannel)
    }

    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes?) {
        super.startElement(uri, localName, qName, attributes)
        if (!isSupportedNamespace(localName, qName)) {
            return;
        }

        when (localName.toLowerCase()) {
            CHANNEL -> currentChannel = iTunesChannel()
            OWNER -> owner = iTunesOwner()
            CATEGORY -> {
                attributes?.getValue(TEXT)?.let {
                    currentChannel?.categories?.add(it)
                }
            }
            IMAGE -> image = attributes?.getValue(HREF)
            ITEM -> currentItem = iTunesItem()
            ENCLOSURE -> enclosure = iTunesEnclosure(
                attributes?.getValue(URL),
                attributes?.getValue(TYPE),
                attributes?.getValue(LENGTH)?.toLong()?: 0
            )
        }
    }

    @Throws(SAXException::class)
    override fun endElement(uri: String, localName: String, qName: String) {
        super.endElement(uri, localName, qName)
        val content = builder.toString().trim()
        if (!isSupportedNamespace(localName, qName)) {
            builder.setLength(0)
            return
        }

        if (currentChannel != null && currentItem == null) {
            when (localName.toLowerCase()) {
                TITLE -> currentChannel?.title = content
                LINK -> currentChannel?.link = content
                LANGUAGE -> currentChannel?.language = content
                COPYRIGHT -> currentChannel?.copyright = content
                SUBTITLE -> currentChannel?.subtitle = content
                AUTHOR -> currentChannel?.author = content
                SUMMARY -> currentChannel?.summary = content
                DESCRIPTION -> currentChannel?.description = content
                NAME -> owner?.name = content
                EMAIL -> owner?.email = content
                OWNER -> owner?.let {
                    currentChannel?.owner = owner
                    owner = null
                }
                IMAGE -> image?.let {
                    currentChannel?.image = image
                    image = null
                }
                EXPLICIT -> currentChannel?.explicit = content.equals("yes", true) ||
                        content.equals("true", true) || content.equals("explicit", true)
            }
        } else if (currentChannel != null && currentItem != null) {
            when (localName.toLowerCase()) {
                TITLE -> currentItem?.title = content
                AUTHOR -> currentItem?.author = content
                DESCRIPTION -> currentItem?.description = content
                SUBTITLE -> currentItem?.subtitle = content
                SUMMARY -> currentItem?.summary = content
                IMAGE -> image?.let {
                    currentItem?.image = image
                    image = null
                }
                ENCLOSURE -> enclosure?.let {
                    currentItem?.enclosure = enclosure
                    enclosure = null
                }
                GUID -> currentItem?.guid = content
                PUB_DATE -> currentItem?.pubDate = content
                DURATION -> currentItem?.duration = content
                EXPLICIT -> currentItem?.explicit = content.equals("yes", true) ||
                        content.equals("true", true) || content.equals("explicit", true)
                IS_CLOSED_CAPTIONED -> currentItem?.isClosedCaptioned = content.equals("yes", true) ||
                        content.equals("true", true) || content.equals("explicit", true)
                ITEM -> currentItem?.let {
                    currentChannel?.items?.add(it)
                    currentItem = null
                }
            }
        }

        builder.setLength(0);
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray?, start: Int, length: Int) {
        super.characters(ch, start, length)
        builder.append(ch, start, length)
    }

    private fun isSupportedNamespace(localName: String, qName: String): Boolean {
        if (localName == qName) {
            return true
        }

        val splits = qName.split(":")
        if (splits.size <= 1) {
            return false
        }

        for (namespace in SUPPORTED_NAMESPACES) {
            if (namespace == splits[0]) {
                return true
            }
        }

        return false
    }

    companion object {
        private val SUPPORTED_NAMESPACES = listOf("itunes")

        // Name
        private const val CHANNEL = "channel"
        private const val ITEM = "item"
        private const val TITLE = "title"
        private const val SUBTITLE = "subtitle"
        private const val SUMMARY = "summary"
        private const val DESCRIPTION = "description"
        private const val LANGUAGE = "language"
        private const val OWNER = "owner"
        private const val NAME = "name"
        private const val EMAIL = "email"
        private const val AUTHOR = "author"
        private const val COPYRIGHT = "copyright"
        private const val LINK = "link"
        private const val IMAGE = "image"
        private const val CATEGORY = "category"
        private const val EXPLICIT = "explicit"
        private const val ENCLOSURE = "enclosure"
        private const val GUID = "guid"
        private const val PUB_DATE = "pubdate"
        private const val DURATION = "duration"
        private const val IS_CLOSED_CAPTIONED = "isclosedcaptioned"

        // Attributes
        private const val TEXT = "text"
        private const val HREF = "href"
        private const val URL = "url"
        private const val LENGTH = "length"
        private const val TYPE = "type"
    }
}