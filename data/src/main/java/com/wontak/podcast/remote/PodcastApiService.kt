package com.wontak.podcast.remote

import com.wontak.podcast.remote.model.rss.iTunesFeed
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface PodcastApiService {

    @GET
    fun get(@Url url: String): Single<iTunesFeed>
}