package com.wontak.podcast.remote.model.atom

import com.google.gson.annotations.SerializedName

data class iTunesEntry(
    @SerializedName("id")
    var id: iTunesLabel,
    @SerializedName("im:name")
    var name: iTunesLabel,
    @SerializedName("im:artist")
    var artist: iTunesLabel,
    @SerializedName("summary")
    var summary: iTunesLabel,
    @SerializedName("im:image")
    var images: List<iTunesLabel>,
    @SerializedName("category")
    var category: iTunesLabel
)