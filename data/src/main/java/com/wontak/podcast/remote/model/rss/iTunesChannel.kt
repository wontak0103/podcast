package com.wontak.podcast.remote.model.rss

data class iTunesChannel(
    var title: String? = null,
    var author: String? = null,
    var subtitle: String? = null,
    var summary: String? = null,
    var description: String? = null,
    var link: String? = null,
    var language: String? = null,
    var copyright: String? = null,
    var owner: iTunesOwner? = null,
    var image: String? = null,
    var explicit: Boolean = false,
    var categories: MutableList<String> = arrayListOf(),
    var items: MutableList<iTunesItem> = arrayListOf()
)