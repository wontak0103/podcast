package com.wontak.podcast.remote.mapper

import com.wontak.podcast.mapper.base.Mapper
import com.wontak.podcast.model.Category
import com.wontak.podcast.model.Chart
import com.wontak.podcast.remote.model.TopPodcasts
import com.wontak.podcast.remote.model.atom.iTunesEntry
import com.wontak.podcast.remote.model.atom.iTunesFeed

class TopPodcastsMapper : Mapper<TopPodcasts, List<Chart>> {

    override fun map(from: TopPodcasts): List<Chart> {
        return map(from.feed)
    }

    private fun map(from: iTunesFeed): List<Chart> {
        return map(from.entries)
    }

    private fun map(from: List<iTunesEntry>): List<Chart> {
        val toReturn = mutableListOf<Chart>()
        for (entry in from) {
            toReturn.add(map(entry))
        }
        return toReturn
    }

    private fun map(from: iTunesEntry): Chart = Chart(
        from.id.attributes.id.toLong(),
        from.name.label,
        from.artist.label,
        from.summary.label,
        from.images[0].label.let { it.substring(0, it.lastIndexOf("/")) }
    )
}