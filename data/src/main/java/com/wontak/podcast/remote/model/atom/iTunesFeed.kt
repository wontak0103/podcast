package com.wontak.podcast.remote.model.atom

import com.google.gson.annotations.SerializedName

data class iTunesFeed(
    @SerializedName("entry")
    var entries: List<iTunesEntry>
)