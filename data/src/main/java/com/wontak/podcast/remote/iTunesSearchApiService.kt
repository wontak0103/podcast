package com.wontak.podcast.remote

import com.wontak.podcast.remote.model.SearchResults
import com.wontak.podcast.remote.model.TopPodcasts
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface iTunesSearchApiService {

    @GET("{countryCode}/rss/topaudiopodcasts/limit={limit}/genre={categoryId}/json")
    fun getTopAudioPodcasts(
        @Path("countryCode") countryCode: String,
        @Path("categoryId") categoryId: Int,
        @Path("limit") limit: Int
    ): Single<TopPodcasts>

    @GET("search")
    fun search(
        @Query("term") keyword: String,
        @Query("limit") limit: Int = 50,
        @Query("entity") entity: String = "podcast"
    ): Single<SearchResults>

    @GET("lookup")
    fun lookup(
        @Query("id") id: Long
    ): Single<SearchResults>
}