package com.wontak.podcast.remote.model.rss

data class iTunesFeed(
    var channel: iTunesChannel? = null
)