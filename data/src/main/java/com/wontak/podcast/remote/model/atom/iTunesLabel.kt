package com.wontak.podcast.remote.model.atom

import com.google.gson.annotations.SerializedName

data class iTunesLabel(
    @SerializedName("label")
    var label: String,
    @SerializedName("attributes")
    var attributes: iTunesAttributes
)