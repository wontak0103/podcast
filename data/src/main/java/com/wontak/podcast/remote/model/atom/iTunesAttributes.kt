package com.wontak.podcast.remote.model.atom

import com.google.gson.annotations.SerializedName

data class iTunesAttributes(
    @SerializedName("im:id")
    var id: String
)