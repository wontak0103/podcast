package com.wontak.podcast.remote.model.rss

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class iTunesRssConverterFactory private constructor() : Converter.Factory() {

    override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?)
            : Converter<ResponseBody, *> = iTunesRssResponseBodyConverter()

    companion object {
        fun create(): iTunesRssConverterFactory = iTunesRssConverterFactory()
    }
}