package com.wontak.podcast.remote.model

import com.google.gson.annotations.SerializedName

data class SearchResults(
    @SerializedName("resultCount")
    var resultCount: Int,
    @SerializedName("results")
    var results: List<SearchResult>
)