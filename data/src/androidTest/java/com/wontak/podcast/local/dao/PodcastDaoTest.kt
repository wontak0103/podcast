package com.wontak.podcast.local.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.wontak.podcast.local.AppDatabaseTest
import com.wontak.podcast.local.model.Podcast
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PodcastDaoTest : AppDatabaseTest() {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun insertAndRead() {
        db.podcastDao().insert(createPodcast(0L, "0"))

        val testObserver = db.podcastDao().load(0L).test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val podcast = testObserver.values()[0]
        Assert.assertThat(podcast, notNullValue())
        Assert.assertThat(podcast.id, `is`(0L))
        Assert.assertThat(podcast.title, `is`("0"))
    }

    fun createPodcast(id: Long, title: String) = Podcast(
        id,
        title,
        "",
        "",
        "",
        "",
        "",
        listOf(),
        false
    )
}