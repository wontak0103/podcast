package com.wontak.podcast.local.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.wontak.podcast.local.AppDatabaseTest
import com.wontak.podcast.local.model.Episode
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EpisodeDaoTest : AppDatabaseTest() {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun insertAndRead() {
        val podcastId = 12345L
        db.episodeDao().insert(createEpisode("0", podcastId))
        db.episodeDao().insert(createEpisode("1", podcastId))
        db.episodeDao().insert(createEpisode("2", podcastId))

        val testObserver = db.episodeDao().load(podcastId).test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val episodes = testObserver.values()[0]
        Assert.assertThat(episodes, notNullValue())
        Assert.assertThat(episodes.size, `is`(3))
        Assert.assertThat(episodes[0].guid, `is`("0"))
        Assert.assertThat(episodes[1].guid, `is`("1"))
        Assert.assertThat(episodes[2].guid, `is`("2"))
    }

    fun createEpisode(guid: String, podcastId: Long) = Episode(
        guid,
        podcastId,
        "test",
        "",
        0L,
        "",
        "",
        "",
        "",
        false
    )
}