package com.wontak.podcast.util

import java.util.regex.Pattern

class DateUtils {

    companion object {

        fun durationStringToMillis(period: String): Long {
            val matcher = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})").matcher(period)
            return if (matcher.matches()) {
                (java.lang.Long.parseLong(matcher.group(1)) * 3600000L
                        + java.lang.Long.parseLong(matcher.group(2)) * 60000
                        + java.lang.Long.parseLong(matcher.group(3)) * 1000
                        + java.lang.Long.parseLong(matcher.group(4)))
            } else {
                throw IllegalArgumentException("Invalid format $period")
            }
        }
    }
}