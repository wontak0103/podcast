package com.wontak.podcast.interactor

import com.wontak.podcast.interactor.base.SingleUseCaseWithParams
import com.wontak.podcast.model.Episode
import com.wontak.podcast.repository.PodcastRepository
import io.reactivex.Single
import java.lang.Exception

class GetEpisodesUseCase constructor(
    private var repository: PodcastRepository
) : SingleUseCaseWithParams<Long, List<Episode>> {

    override fun execute(params: Long): Single<List<Episode>> {
        return Single.error(Exception())
    }
}