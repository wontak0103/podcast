package com.wontak.podcast.interactor.base

import io.reactivex.Observable

interface UseCaseWithParams<in P, T> {

    fun execute(params: P): Observable<T>
}
