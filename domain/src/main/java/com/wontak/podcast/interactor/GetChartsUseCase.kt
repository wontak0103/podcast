package com.wontak.podcast.interactor

import com.wontak.podcast.interactor.base.SingleUseCaseWithParams
import com.wontak.podcast.model.Category
import com.wontak.podcast.model.Chart
import com.wontak.podcast.repository.ChartRepository
import io.reactivex.Single

class GetChartsUseCase constructor(
    private var repository: ChartRepository
) : SingleUseCaseWithParams<Category, List<Chart>> {

    override fun execute(params: Category): Single<List<Chart>> {
        TODO("not implemented")
    }
}