package com.wontak.podcast.interactor.base

import io.reactivex.Single

interface SingleUseCaseWithParams<in P, R> {

    fun execute(params: P): Single<R>
}