package com.wontak.podcast.interactor

import com.wontak.podcast.interactor.base.SingleUseCaseWithParams
import com.wontak.podcast.model.Podcast
import com.wontak.podcast.repository.PodcastRepository
import io.reactivex.Single

class GetPodcastsUseCase constructor(
    private var repository: PodcastRepository
) : SingleUseCaseWithParams<String, List<Podcast>> {

    override fun execute(params: String): Single<List<Podcast>> {
        TODO("not implemented")
    }
}