package com.wontak.podcast.interactor.base

import io.reactivex.Completable

interface CompletableUseCase {

    fun execute(): Completable
}