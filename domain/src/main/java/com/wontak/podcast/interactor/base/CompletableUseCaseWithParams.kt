package com.wontak.podcast.interactor.base

import io.reactivex.Completable

interface CompletableUseCaseWithParams<P> {

    fun execute(params: P): Completable
}