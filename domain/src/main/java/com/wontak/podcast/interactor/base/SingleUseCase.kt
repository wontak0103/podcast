package com.wontak.podcast.interactor.base

import io.reactivex.Single

interface SingleUseCase<R> {

    fun execute(): Single<R>
}
