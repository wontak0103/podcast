package com.wontak.podcast.interactor

import com.wontak.podcast.interactor.base.SingleUseCaseWithParams
import com.wontak.podcast.model.Podcast
import com.wontak.podcast.repository.PodcastRepository

class GetPodcastUseCase constructor(
    private var repository: PodcastRepository
) : SingleUseCaseWithParams<Long, Podcast> {

    override fun execute(params: Long) = repository.getPodcast(params)
}