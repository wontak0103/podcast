package com.wontak.podcast.interactor.base

import io.reactivex.Observable

interface UseCase<R> {

    fun execute(): Observable<R>
}