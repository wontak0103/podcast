package com.wontak.podcast.repository

import com.wontak.podcast.model.Category
import com.wontak.podcast.model.Chart
import io.reactivex.Single

interface ChartRepository {

    fun getTopAudioPodcasts(contryCode: String, category: Category, limit: Int): Single<List<Chart>>
}
