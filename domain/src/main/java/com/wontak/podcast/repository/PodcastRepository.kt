package com.wontak.podcast.repository

import com.wontak.podcast.model.Podcast
import io.reactivex.Single

interface PodcastRepository {

    fun getPodcast(id: Long): Single<Podcast>
}