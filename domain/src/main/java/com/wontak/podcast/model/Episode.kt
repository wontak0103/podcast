package com.wontak.podcast.model

data class Episode(
    var guid: String,
    var podcastId: Long,
    var title: String,
    var description: String,
    var duration: Long,
    var updatedAt: Long,
    var explicit: Boolean,
    var fileUrl: String,
    var fileType: String
)