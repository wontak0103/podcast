package com.wontak.podcast.model

data class Feature(
    var title: String,
    var desc: String,
    var imageUrl: String
)