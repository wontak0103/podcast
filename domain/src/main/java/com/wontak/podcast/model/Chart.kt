package com.wontak.podcast.model

data class Chart(
    var id: Long,
    var title: String,
    var auther: String,
    var summary: String,
    var imageBaseUrl: String
)