package com.wontak.podcast.model

data class Podcast(
    var id: Long,
    var title: String,
    var author: String,
    var description: String,
    var language: String,
    var link: String,
    var imageUrl: String,
    var categories: List<Category>,
    var explicit: Boolean
)