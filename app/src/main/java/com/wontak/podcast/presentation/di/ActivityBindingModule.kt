package com.wontak.podcast.presentation.di

import com.wontak.podcast.presentation.ui.main.MainActivity
import com.wontak.podcast.presentation.ui.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivity(): MainActivity
}
