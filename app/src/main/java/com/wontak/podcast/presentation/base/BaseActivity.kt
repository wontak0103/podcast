package com.wontak.podcast.presentation.base

import dagger.android.x.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()
