package com.wontak.podcast.presentation

import com.wontak.podcast.presentation.base.BaseApplication
import com.wontak.podcast.presentation.di.DaggerAppComponent
import dagger.android.AndroidInjector

class AndroidApplication : BaseApplication() {

    override fun applicationInjector(): AndroidInjector<BaseApplication> {
        return DaggerAppComponent.builder()
            .create(this)
    }
}