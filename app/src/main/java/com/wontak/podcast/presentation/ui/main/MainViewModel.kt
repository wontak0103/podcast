package com.wontak.podcast.presentation.ui.main

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class MainViewModel
@Inject constructor() : ViewModel() {

    val counter = MutableLiveData<Int>()

    var number = 0

    fun restoreInstanceState(savedInstanceState: Bundle?) {}

    fun onButtonClick() = counter.setValue(++number)
}