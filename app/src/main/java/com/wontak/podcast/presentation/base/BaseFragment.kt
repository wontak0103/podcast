package com.wontak.podcast.presentation.base

import dagger.android.x.DaggerFragment

abstract class BaseFragment : DaggerFragment()