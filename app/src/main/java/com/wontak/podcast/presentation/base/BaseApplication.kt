package com.wontak.podcast.presentation.base

import dagger.android.x.DaggerApplication

abstract class BaseApplication : DaggerApplication()
