package com.wontak.podcast.presentation.di

import com.wontak.podcast.presentation.base.BaseApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.x.AndroidXInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidXInjectionModule::class,
    AppModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class
])
interface AppComponent: AndroidInjector<BaseApplication> {

    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<BaseApplication>()
}
